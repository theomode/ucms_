from django.shortcuts import render
# from collections import defaultdict
from people.models import People

from models import Blurb

from django.core import serializers
import json

from django.contrib.sites.shortcuts import get_current_site

# Create your views here.
def map(request):
	people = People.objects.exclude(location__isnull=True)

	geodata = {}
	entry_list = []
	for p in people:
		entry = {
			'first_name': p.first_name,
			'last_name' : p.last_name, 
			'country': p.location.country, 
			'city': p.location.city, 
			'longitude' : p.location.longitude,
			'latitude':  p.location.latitude}
		entry_list.append(entry)
	geodata['ppl'] = entry_list
	
	project = get_current_site(request)
	try: 
		blurb = Blurb.objects.get(project=project).text
	except Blurb.DoesNotExist:
		blurb = ''

	# geodata = serializers.serialize("json", people, fields=('location','first_name'))
	# values = defaultdict(list)
	# test = map(lambda people: values[people.location].append(people), qs)
	context = {'geodata': json.dumps(geodata), 'blurb': blurb}


	return render(request, 'map/map.html', context)

