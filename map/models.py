from django.db import models

from django.contrib.sites.models import Site

# Create your models here.
class Blurb(models.Model):
	project = models.ForeignKey(Site, related_name="blurbprob")
	text = models.TextField(blank=True, null=True, help_text="ckeditor") 
	
	def __unicode__(self):
		return self.project.name

