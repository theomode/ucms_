from django.contrib import admin
from models import Blurb
from django.db import models
from django import forms
import reversion
# Register your models here.

class BlurbAdmin(reversion.VersionAdmin):
	list_filter = ['project']
	
	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})},}
	class Media:
		js = ('ckeditor/ckeditor.js','admin/admin.js',)


	
	
admin.site.register(Blurb, BlurbAdmin)