from django.contrib import admin
from videos.models import Video, Featured

import reversion


class VideoAdmin(reversion.VersionAdmin):
	
	search_fields = ['title', 'body']
	list_filter = [ 'project']
	filter_horizontal = ('project',)

class FeaturedAdmin(reversion.VersionAdmin):
	list_filter = ['project']

admin.site.register(Video,VideoAdmin)
admin.site.register(Featured,FeaturedAdmin)
