from django.db import models
from django.contrib.sites.models import Site

class Video(models.Model):
	VIDEO_SOURCE_CHOICES = (
	    ('VM', 'Vimeo'),
	    ('YT', 'Youtube'),
	)
	title = models.CharField(max_length=255,blank=True, null=True)
	video_source = models.CharField(max_length=255, choices=VIDEO_SOURCE_CHOICES, default="YT")
	video_code = models.CharField(max_length=255,blank=True, null=True)
	project		= models.ManyToManyField(Site,blank=True, null=True,related_name="vid proj")

	def __unicode__(self):
		return self.title

class Featured(models.Model):
	feat = models.ForeignKey(Video)
	project		= models.ManyToManyField(Site,blank=True, null=True,related_name="feat proj")

	def get_video_source(self):
		return self.feat.video_source 

	def get_video_code(self):
		return self.feat.video_code
	def __unicode__(self):
		return self.feat.title
