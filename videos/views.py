from django.shortcuts import render
from videos.models import Video, Featured

from django.contrib.sites.shortcuts import get_current_site


def videos(request):
	project = get_current_site(request)
	videos = Video.objects.filter(project=project)
	featured = Featured.objects.filter(project=project)

	context = { "videos" : videos, "featured" : featured  }
	return render(request, 'videos/videos.html', context)

