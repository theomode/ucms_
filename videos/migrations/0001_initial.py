# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Featured',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, null=True, blank=True)),
                ('video_source', models.CharField(default=b'YT', max_length=255, choices=[(b'VM', b'Vimeo'), (b'YT', b'Youtube')])),
                ('video_code', models.CharField(max_length=255, null=True, blank=True)),
                ('project', models.ManyToManyField(related_name='vid proj', null=True, to='sites.Site', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='featured',
            name='feat',
            field=models.ForeignKey(to='videos.Video'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='featured',
            name='project',
            field=models.ManyToManyField(related_name='feat proj', null=True, to='sites.Site', blank=True),
            preserve_default=True,
        ),
    ]
