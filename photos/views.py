from django.shortcuts import render
from photos.models import Album, Gallery, Row, Image, Feat, RowImage
from splash.models import Slideshow, Slide
# Create your views here.
from django.contrib.sites.shortcuts import get_current_site

from django.http import Http404

def photos(request):
	project = get_current_site(request)
	try:
		albums = Album.objects.filter(project=project)
	except:
		raise Http404 
	try:
		feat = Feat.objects.filter(project=project)[0].album
	except IndexError:
		raise Http404

	context = { "feat": feat, "albums" : albums }
	return render(request, 'photos/photos.html', context)


def albums(request,album):
	project = get_current_site(request)
	try:
		albs = Album.objects.get(page_url=album,project=project)
	except: 
		raise Http404

	size = albs.galleries.count()
	albums = ''
	if size <= 0:
		raise Http404
	elif size == 1:
		url = albs.galleries.all()[0].page_url
		return gallery(request, url)
	else: 
		albums = albs.galleries.all()

	context = { "albums" : albums, "name": albs.title, 'count': size}
	
	return render(request, 'photos/albums.html', context)


def gallery(request,gallery=None):
	project = get_current_site(request)
	gallery = Gallery.objects.get(project=project,page_url=gallery)
	row_obj = Row.objects.filter(gallery=gallery)
	title = gallery.title
	
	rows = []
	for row in row_obj:
		images = RowImage.objects.filter(row=row)
		row.add_images(images)
		rows.append(row)

	context = {'rows':rows, 'title':title}
	
	return render(request, 'photos/gallery.html', context)