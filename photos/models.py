from django.db import models
from s3direct.fields import S3DirectField
from people.models import People

from django.contrib.sites.models import Site

# Create your models here.
class Image(models.Model):
	title = models.CharField(max_length=255)
	alt_text = models.CharField(max_length=255,blank=True,null=True)
	image = S3DirectField(dest='imgs', help_text="<p>Width:1920px Height:800px</p>") 
	assoc_people = models.ManyToManyField(People, related_name="associates", blank=True, null=True)

	def __unicode__(self):
		return self.title

class Gallery(models.Model):
	title = models.CharField(max_length=255,)
	page_url = models.SlugField()
	square_cover = S3DirectField(dest='imgs', help_text="<p>Width:500px Height:500px</p>")
	
	project = models.ForeignKey(Site,null=True)

	def __unicode__(self):
		return self.title 

	def get_absolute_url(self):
		return  '/photos/gallery' + self.page_url

class Row(models.Model):
	Quarter = 'Q'
	Thirds = 'T'
	Half = 'H'
	Full = 'F'

	ROW_CHOICES = (
	    ('Q', 'Quarter'),
	    ('T', 'Thirds'),
	    ('H', 'Half'),
	    ('F', 'Full'),
	)

	gallery = models.ForeignKey(Gallery)
	name = models.CharField(max_length=255)
	row_choice = models.CharField(max_length=255, choices=ROW_CHOICES, default=Full)
	
	def add_images(self,images):
		self.images = images

	def __unicode__(self):
		return self.name

class RowImage(models.Model):
	row = models.ForeignKey(Row)
	title = models.CharField(max_length=255)
	alt_text = models.CharField(max_length=255,blank=True,null=True)
	image = S3DirectField(dest='imgs', help_text="<p>Width:1920px Height:800px</p>") 
	position = models.IntegerField(default=0)

	def __unicode__(self):
		return self.title

	class Meta:
		ordering = ['position']


class Album(models.Model):
	feat_image	= S3DirectField(dest='imgs', blank=True, help_text="<p>Width:1920px Height:800px</p>")
	square_cover = S3DirectField(dest='imgs', blank=True, help_text="<p>Width:500px Height:500px</p>") 
	title = models.CharField(max_length=255)
	galleries = models.ManyToManyField(Gallery, related_name="meta album")
	page_url = models.SlugField()
	project = models.ForeignKey(Site,null=True)
	assoc_people = models.ManyToManyField(People, related_name="photogalppl", blank=True, null=True)

	def __unicode__(self):
		return self.title

	def get_absolute_url(self):
		return  '/photos/' + self.page_url

class Feat(models.Model):
	project = models.ForeignKey(Site, related_name="feat site")
	album = models.ForeignKey(Album, related_name="featured post")
	people = models.ManyToManyField(People, related_name="featured people",blank=True, null=True)

	def __unicode__(self):
		return self.project.name


