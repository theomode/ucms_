# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photos', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='gallery',
            name='row',
        ),
        migrations.AddField(
            model_name='row',
            name='gallery',
            field=models.ForeignKey(default='', to='photos.Gallery'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='row',
            name='images',
            field=models.ManyToManyField(to='photos.Image'),
            preserve_default=True,
        ),
    ]
