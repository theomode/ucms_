# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import s3direct.fields


class Migration(migrations.Migration):

    dependencies = [
        ('photos', '0005_auto_20150404_1534'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='moment',
            name='gallery',
        ),
        migrations.RemoveField(
            model_name='moment',
            name='project',
        ),
        migrations.RemoveField(
            model_name='album',
            name='moments',
        ),
        migrations.DeleteModel(
            name='Moment',
        ),
        migrations.AddField(
            model_name='album',
            name='galleries',
            field=models.ManyToManyField(related_name='meta album', to='photos.Gallery'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='gallery',
            name='square_cover',
            field=s3direct.fields.S3DirectField(default='', help_text=b'<p>Width:500px Height:500px</p>'),
            preserve_default=False,
        ),
    ]
