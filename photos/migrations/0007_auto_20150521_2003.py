# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import s3direct.fields


class Migration(migrations.Migration):

    dependencies = [
        ('photos', '0006_auto_20150420_2138'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='featured',
            name='assoc_people',
        ),
        migrations.RemoveField(
            model_name='featured',
            name='images',
        ),
        migrations.AddField(
            model_name='featured',
            name='feat_link',
            field=models.URLField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='featured',
            name='image',
            field=s3direct.fields.S3DirectField(default='', help_text=b'<p>Width:1920px Height:800px</p>'),
            preserve_default=False,
        ),
    ]
