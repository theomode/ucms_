# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0002_people_snapshot_image'),
        ('sites', '0001_initial'),
        ('photos', '0007_auto_20150521_2003'),
    ]

    operations = [
        migrations.CreateModel(
            name='Feat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('album', models.ForeignKey(related_name='featured post', to='photos.Album')),
                ('people', models.ManyToManyField(related_name='featured people', null=True, to='people.People', blank=True)),
                ('project', models.ForeignKey(related_name='feat site', to='sites.Site')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='featured',
            name='project',
        ),
        migrations.DeleteModel(
            name='Featured',
        ),
    ]
