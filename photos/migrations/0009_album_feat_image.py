# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import s3direct.fields


class Migration(migrations.Migration):

    dependencies = [
        ('photos', '0008_auto_20150521_2020'),
    ]

    operations = [
        migrations.AddField(
            model_name='album',
            name='feat_image',
            field=s3direct.fields.S3DirectField(help_text=b'<p>Width:1920px Height:800px</p>', blank=True),
            preserve_default=True,
        ),
    ]
