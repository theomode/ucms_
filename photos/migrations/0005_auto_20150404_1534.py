# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0001_initial'),
        ('photos', '0004_auto_20150330_2209'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='rowimage',
            name='assoc_people',
        ),
        migrations.AddField(
            model_name='album',
            name='assoc_people',
            field=models.ManyToManyField(related_name='photogalppl', null=True, to='people.People', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='featured',
            name='assoc_people',
            field=models.ManyToManyField(related_name='photofeatppl', null=True, to='people.People', blank=True),
            preserve_default=True,
        ),
    ]
