# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import s3direct.fields


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0001_initial'),
        ('photos', '0002_auto_20150330_2155'),
    ]

    operations = [
        migrations.CreateModel(
            name='RowImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('alt_text', models.CharField(max_length=255, null=True, blank=True)),
                ('image', s3direct.fields.S3DirectField(help_text=b'<p>Width:1920px Height:800px</p>')),
                ('assoc_people', models.ManyToManyField(related_name='row_img_assoc', null=True, to='people.People', blank=True)),
                ('row', models.ForeignKey(to='photos.Row')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='row',
            name='images',
        ),
    ]
