# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photos', '0003_auto_20150330_2208'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='rowimage',
            options={'ordering': ['position']},
        ),
        migrations.AddField(
            model_name='rowimage',
            name='position',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
