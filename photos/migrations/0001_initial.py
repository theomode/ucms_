# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import s3direct.fields


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
        ('people', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Album',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('square_cover', s3direct.fields.S3DirectField(help_text=b'<p>Width:500px Height:500px</p>', blank=True)),
                ('title', models.CharField(max_length=255)),
                ('page_url', models.SlugField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Featured',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Gallery',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('page_url', models.SlugField()),
                ('project', models.ForeignKey(to='sites.Site', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('alt_text', models.CharField(max_length=255, null=True, blank=True)),
                ('image', s3direct.fields.S3DirectField(help_text=b'<p>Width:1920px Height:800px</p>')),
                ('assoc_people', models.ManyToManyField(related_name='associates', null=True, to='people.People', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Moment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('square_cover', s3direct.fields.S3DirectField(help_text=b'<p>Width:500px Height:500px</p>')),
                ('title', models.CharField(max_length=255)),
                ('gallery', models.ForeignKey(related_name='galleries', to='photos.Gallery', help_text=b'overriden if album chosen')),
                ('project', models.ForeignKey(to='sites.Site', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Row',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('row_choice', models.CharField(default=b'F', max_length=255, choices=[(b'Q', b'Quarter'), (b'T', b'Thirds'), (b'H', b'Half'), (b'F', b'Full')])),
                ('images', models.ManyToManyField(related_name='photos', to='photos.Image')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='gallery',
            name='row',
            field=models.ManyToManyField(related_name='rows', to='photos.Row'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='featured',
            name='images',
            field=models.ManyToManyField(to='photos.Image'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='featured',
            name='project',
            field=models.ForeignKey(related_name='feat photo', to='sites.Site', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='album',
            name='moments',
            field=models.ManyToManyField(related_name='meta album', to='photos.Moment'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='album',
            name='project',
            field=models.ForeignKey(to='sites.Site', null=True),
            preserve_default=True,
        ),
    ]
