from django.conf.urls import patterns, url
from photos import views
#from views import display_tag, display_slug, archive_list, search



urlpatterns = patterns('',
	url(r'^$', views.photos),
	url(r'^gallery/(?P<gallery>.+)', views.gallery),
	url(r'^(?P<album>.+)/', views.albums),
	
	
)