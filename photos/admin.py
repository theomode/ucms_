from django.contrib import admin
from django.db import models
from photos.models import Image, Row, Gallery, Album, Feat, RowImage
import reversion


from django.conf.urls import patterns
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect


class ImageAdmin(reversion.VersionAdmin):
	filter_horizontal = ('assoc_people',)
class GalleryAdmin(reversion.VersionAdmin):
	def get_urls(self):
		urls = super(GalleryAdmin, self).get_urls()
		my_urls = patterns('',
			(r'(?P<id>[0-9]+)/preview/$', self.admin_site.admin_view(self.preview)), # blog post preview
		)
		return my_urls + urls


	def preview(self, request, id):
		gallery = get_object_or_404(Gallery, pk=id)
		row_obj = Row.objects.filter(gallery=gallery)
		title = gallery.title
	
		rows = []
		for row in row_obj:
			images = RowImage.objects.filter(row=row)
			row.add_images(images)
			rows.append(row)

		context = {'rows':rows, 'title':title}

		return render_to_response('photos/gallery.html', context, context_instance=RequestContext(request))

	# add 'preview' post-save option
	def response_change(self, request, obj):
		res = super(GalleryAdmin, self).response_change(request, obj)
		if "_preview" in request.POST:
			storage = messages.get_messages(request)
			storage.used = True
			return HttpResponseRedirect('/admin/photos/gallery/%s/preview' % (obj.pk,))
		else:
			return res
			

	search_fields = ['title', 'project']
	list_filter = ['project']
	

class AlbumAdmin(reversion.VersionAdmin):
	list_display = ['title', 'project', 'id']
	search_fields = ['title']
	list_filter = ['project']
	
class FeatAdmin(reversion.VersionAdmin):
	filter_horizontal = ('people',)
	
	def get_queryset(self, request):
		qs = Feat.objects.all()
		if request.user.is_superuser:
			return qs
		else:
			sites = []
	    	for s in request.user.groups.all():
	    		try:
	    			s = Site.objects.get(name=s)
	    		except Site.DoesNotExist:
	    			s = ''
	    		sites.append(s)
			return qs.filter(project__in=sites).distinct()

class RowImageAdmin(reversion.VersionAdmin):
	search_fields = ['title']
	list_filter = ['assoc_people']
	filter_horizontal = ('assoc_people')

class RowImageInline(admin.TabularInline):
	model = RowImage
	extra = 1

class RowAdmin(reversion.VersionAdmin):
	list_display = ['name', 'gallery', 'row_choice']
	search_fields = ['name']
	list_filter = ['gallery','row_choice']
	inlines = [RowImageInline]


admin.site.register(Feat,FeatAdmin)
admin.site.register(Album,AlbumAdmin)
admin.site.register(Gallery,GalleryAdmin)
admin.site.register(Row,RowAdmin)
admin.site.register(Image,ImageAdmin)
