import reversion
from django.contrib import admin
from django.db import models
from splash.models import  Slideshow, Slide, Splash, RowImage, Partner

from django.contrib.sites.shortcuts import get_current_site

from django.conf.urls import patterns
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django import forms
from django.http import HttpResponseRedirect


class RowImageInline(admin.TabularInline):
	model = RowImage
	extra = 1

class PartnerAdmin(reversion.VersionAdmin):
	inlines = [RowImageInline]



class SlideInline(admin.TabularInline):
    model = Slide
    extra = 1


class SlideshowAdmin(reversion.VersionAdmin):
	filter_horizontal = ('people',)
	inlines = [SlideInline]

	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})},}
	class Media:
		js = ('ckeditor/ckeditor.js','admin/admin.js',)



class SplashAdmin(reversion.VersionAdmin):
	def get_urls(self):
		urls = super(SplashAdmin, self).get_urls()
		my_urls = patterns('',
			(r'(?P<id>[0-9]+)/preview/$', self.admin_site.admin_view(self.preview)), # blog post preview
		)
		return my_urls + urls


	def preview(self, request, id):
		project = get_current_site(request)
		post = get_object_or_404(Splash, pk=id)
		splash = get_object_or_404(Splash, pk=id)
		slides = Slide.objects.filter(slideshow=splash.slideshow)
		posts = Post.objects.filter(project=project)[:6]
		row_obj = Partner.objects.filter(splash=splash)
		partners = []
		for row in row_obj:
			images = RowImage.objects.filter(row=row)
			row.add_images(images)
			partners.append(row)

		context = {'splash':splash, 'posts':posts, 'slides':slides, 'partners': partners}
		
		return render_to_response('pages/splash.html', context, context_instance=RequestContext(request))

	# add 'preview' post-save option
	def response_change(self, request, obj):
		res = super(SplashAdmin, self).response_change(request, obj)
		if "_preview" in request.POST:
			storage = messages.get_messages(request)
			storage.used = True
			return HttpResponseRedirect('/admin/blog/post/%s/preview' % (obj.pk,))
		else:
			return res
	
	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})},}
	class Media:
		js = ('ckeditor/ckeditor.js','admin/admin.js',)




	list_display = ['name', 'slideshow', 'project', 'id']
	search_fields = ['name']
	list_filter = ['project']
	
admin.site.register(Slideshow, SlideshowAdmin)
admin.site.register(Partner,PartnerAdmin)
admin.site.register(Splash,SplashAdmin)
