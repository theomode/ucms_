from django.db import models
from s3direct.fields import S3DirectField
from django import forms
from people.models import People
from universe.models import Link

from django.contrib.sites.models import Site

class Slideshow(models.Model):
	name = models.CharField(max_length=255)
	people		= models.ManyToManyField(People, related_name="slidepeople",blank=True, null=True)
	project 	= models.ForeignKey(Site)
	
	def __unicode__(self):
		return self.name

class Splash(models.Model):
	name 		= models.CharField(max_length=255)
	slideshow 	= models.ForeignKey(Slideshow,default='',null=False,blank=True)
	quote 		= models.TextField(help_text="ckeditor",blank=True, null=True)
	show_posts	= models.BooleanField(default=False)
	project 	= models.ForeignKey(Site)
	
	def __unicode__(self):
		return self.name

class Slide(models.Model):
	slide_name	= models.CharField(max_length=255,default="slide")
	title 		= models.CharField(max_length=255,blank=True, null=True)
	pic			= S3DirectField(dest='imgs', blank=True, help_text="<p>Width:560px Height:300px</p>")
	text		= models.TextField(blank=True, null=True, help_text="ckeditor")
	button		= models.ForeignKey(Link,null=True,blank=True)
	position	= models.IntegerField(default=1)
	slideshow 	= models.ForeignKey(Slideshow)

	def __unicode__(self):
		return self.slide_name

	class Meta:
		ordering = ['position']

class Partner(models.Model):

	ROW_CHOICES = (
	    ('Q', 'Quarter'),
	    ('T', 'Thirds'),
	    ('H', 'Half'),
	    ('F', 'Full'),
	    ('S', 'Sixth'),
	)

	splash = models.ForeignKey(Splash)
	name = models.CharField(max_length=255)
	row_choice = models.CharField(max_length=255, choices=ROW_CHOICES, default='Full')
	
	def add_images(self,images):
		self.images = images

	def __unicode__(self):
		return self.name

class RowImage(models.Model):
	row = models.ForeignKey(Partner)
	title = models.CharField(max_length=255)
	url = models.URLField(blank=True,null=True)
	alt_text = models.CharField(max_length=255,blank=True,null=True)
	image = S3DirectField(dest='imgs', help_text="<p>Width:1920px Height:800px</p>") 
	assoc_people = models.ManyToManyField(People, related_name="partner ppl", blank=True, null=True)
	position = models.IntegerField(default=0)

	def __unicode__(self):
		return self.title

	class Meta:
		ordering = ['position']

