# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('splash', '0013_auto_20150420_2140'),
    ]

    operations = [
        migrations.RenameField(
            model_name='slide',
            old_name='new',
            new_name='title',
        ),
    ]
