# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('splash', '0011_merge'),
        ('splash', '0011_rowimage_url'),
    ]

    operations = [
    ]
