# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import s3direct.fields


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0001_initial'),
        ('splash', '0003_auto_20150331_0430'),
    ]

    operations = [
        migrations.AddField(
            model_name='slideshow',
            name='people',
            field=models.ManyToManyField(related_name='slidepeople', null=True, to='people.People', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='splash',
            name='people',
            field=models.ManyToManyField(related_name='splashpeople', null=True, to='people.People', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='slide',
            name='pic',
            field=s3direct.fields.S3DirectField(help_text=b'<p>Width:560px Height:300px</p>', blank=True),
            preserve_default=True,
        ),
    ]
