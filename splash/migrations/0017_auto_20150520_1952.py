# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('splash', '0016_auto_20150508_1404'),
    ]

    operations = [
        migrations.AlterField(
            model_name='slide',
            name='text',
            field=models.TextField(help_text=b'ckeditor', null=True, blank=True),
            preserve_default=True,
        ),
    ]
