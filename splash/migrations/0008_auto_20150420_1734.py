# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('splash', '0007_auto_20150420_1715'),
    ]

    operations = [
        migrations.AlterField(
            model_name='slide',
            name='slide_name',
            field=models.CharField(default=b'slide', max_length=255),
            preserve_default=True,
        ),
    ]
