# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('splash', '0006_slideshow_project'),
    ]

    operations = [
        migrations.AddField(
            model_name='slide',
            name='slide_name',
            field=models.CharField(max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='slide',
            name='text',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='splash',
            name='quote',
            field=models.TextField(help_text=b'ckeditor', null=True, blank=True),
            preserve_default=True,
        ),
    ]
