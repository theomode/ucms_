# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import s3direct.fields


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0001_initial'),
        ('splash', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Row',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('row_choice', models.CharField(default=b'Full', max_length=255, choices=[(b'Q', b'Quarter'), (b'T', b'Thirds'), (b'H', b'Half'), (b'F', b'Full'), (b'S', b'Sixth')])),
                ('splash', models.ForeignKey(to='splash.Splash')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RowImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('alt_text', models.CharField(max_length=255, null=True, blank=True)),
                ('image', s3direct.fields.S3DirectField(help_text=b'<p>Width:1920px Height:800px</p>')),
                ('position', models.IntegerField(default=0)),
                ('assoc_people', models.ManyToManyField(related_name='partner ppl', null=True, to='people.People', blank=True)),
                ('row', models.ForeignKey(to='splash.Row')),
            ],
            options={
                'ordering': ['position'],
            },
            bases=(models.Model,),
        ),
    ]
