# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('splash', '0015_remove_splash_people'),
    ]

    operations = [
        migrations.AlterField(
            model_name='slide',
            name='button',
            field=models.ForeignKey(blank=True, to='universe.Link', null=True),
            preserve_default=True,
        ),
    ]
