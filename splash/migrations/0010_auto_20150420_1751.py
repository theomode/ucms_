# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('splash', '0009_auto_20150420_1740'),
    ]

    operations = [
        migrations.RenameField(
            model_name='slide',
            old_name='name',
            new_name='title',
        ),
    ]
