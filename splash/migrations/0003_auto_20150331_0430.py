# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('splash', '0002_row_rowimage'),
    ]

    operations = [
        migrations.CreateModel(
            name='Partner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('row_choice', models.CharField(default=b'Full', max_length=255, choices=[(b'Q', b'Quarter'), (b'T', b'Thirds'), (b'H', b'Half'), (b'F', b'Full'), (b'S', b'Sixth')])),
                ('splash', models.ForeignKey(to='splash.Splash')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='row',
            name='splash',
        ),
        migrations.AlterField(
            model_name='rowimage',
            name='row',
            field=models.ForeignKey(to='splash.Partner'),
            preserve_default=True,
        ),
        migrations.DeleteModel(
            name='Row',
        ),
    ]
