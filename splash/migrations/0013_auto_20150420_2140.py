# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('splash', '0012_merge'),
    ]

    operations = [
        migrations.RenameField(
            model_name='slide',
            old_name='title',
            new_name='new',
        ),
    ]
