# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('splash', '0014_auto_20150420_2140'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='splash',
            name='people',
        ),
    ]
