# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('splash', '0010_auto_20150420_1751'),
        ('splash', '0010_merge'),
    ]

    operations = [
    ]
