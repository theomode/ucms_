from people.models import Mode
from menu.models import Menu, MenuItem, Submenu
from universe.models import Link, Theme
from footer.models import Footer, SocialMedia

from django.contrib.sites.shortcuts import get_current_site


def initial(request):
    
    project = get_current_site(request)
    path = getSub(request.path)
   
    try:
        menu = Menu.objects.get(project=project)
        nav = menu.get_nav_html(project,path)
    except Menu.DoesNotExist:
        menu = ''
        nav = ''
    
    try:
    	footer = Footer.objects.get(project=project)
        footer.images = SocialMedia.objects.filter(footer=footer)
    except Footer.DoesNotExist:
    	footer = ''

    
    try:
        link = Link.objects.filter(url=path)[0]
        try: 
            sub = Submenu.objects.filter(project=project,links=link)[0]
        except IndexError:
            sub = ''
    except IndexError:
        sub = ''

    try:
        theme = Theme.objects.get(project=project)
    except Theme.DoesNotExist:
        theme = ''

    return { 'footer': footer , 'menu': menu, 'theme': theme, 'nav': nav, 'sub':sub, 'path':path[:]}

def getSub(path):
    path = path[1:]
    try: 
        path = path[:path.index('/')]
    except ValueError:
        return ''
    return '/' + path

