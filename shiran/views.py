from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, Http404

from django.template import RequestContext
import time

from django.shortcuts import render_to_response
from django.contrib.sites.shortcuts import get_current_site

from splash.models import Splash, Slide, Partner, RowImage
from blog.models import Post


### Login Page ###
"""
Login Page;
Enter credentials to be processed by the projects page
"""
def login(request):
	return render(request, 'login.html')
"""
Logout of session; 
remove session variables and return to login page
"""
def logout(request):
	
	for state, sessionInfo in request.session.items():
		sessionInfo = None
	return HttpResponseRedirect('/login/')

'''
Load splash
'''
def splash(request):
	project = get_current_site(request)
	try:
		splash = Splash.objects.filter(project=project)[0]
		slides = Slide.objects.filter(slideshow=splash.slideshow)
	except IndexError:
		raise Http404
	posts = Post.objects.filter(project=project)[:6]
	row_obj = Partner.objects.filter(splash=splash)
	partners = []
	for row in row_obj:
		images = RowImage.objects.filter(row=row)
		row.add_images(images)
		partners.append(row)

	context = {'splash':splash, 'posts':posts, 'slides':slides, 'partners': partners}
	return render_to_response('pages/splash.html', context, context_instance=RequestContext(request))
	


