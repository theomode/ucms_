from django.contrib import sitemaps
from django.contrib.sites.models import Site
from django.contrib.sitemaps import GenericSitemap

from blog.models import Post, Tag, Category
from events.models import Event, Type
from music.models import Wave
from pages.models import Page
from people.models import Mode, People
from photos.models import Gallery, Album

PRJ = Site.objects.get_current()


post_dict = {
    'queryset': Post.objects.filter(project=PRJ),
    'date_field': 'date',
}

tag_dict = {
    'queryset': Tag.objects.all(),
}

category_dict = {
    'queryset': Category.objects.all(),
}

event_dict = {
    'queryset': Event.objects.filter(project=PRJ),
    'date_field': 'date',
}

type_dict = {
    'queryset': Type.objects.all(),
}

wave_dict = {
	'queryset': Wave.objects.filter(project=PRJ),
	'date_field': 'date',
}

page_dict = {
	'queryset': Page.objects.filter(project=PRJ),		
}

roles_dict = {
	'queryset': Mode.objects.filter(project=PRJ)
}
pks = []

for m in Mode.objects.filter(project=PRJ):
	pks.append(m.person.pk)

ppl_dict = {
	'queryset': People.objects.filter(pk__in=pks)
}

gallery_dict = {
	'queryset': Gallery.objects.filter(project=PRJ)
}

album_dict = {
	'queryset': Gallery.objects.filter(project=PRJ)
}


sitemaps = {
    'post': GenericSitemap(post_dict, priority=0.6),
    'tag': GenericSitemap(tag_dict, priority=0.6),
    'category': GenericSitemap(category_dict, priority=0.6),
    'event_dict' : GenericSitemap(event_dict, priority=0.6),
	'type_dict' : GenericSitemap(type_dict, priority=0.6),	
	'page_dict' : GenericSitemap(page_dict, priority=0.6),	
	'roles_dict' : GenericSitemap(roles_dict, priority=0.6),	
	'ppl_dict' : GenericSitemap(ppl_dict, priority=0.6),	
	'gallery_dict' : GenericSitemap(gallery_dict, priority=0.6),	
	'album_dict' : GenericSitemap(album_dict, priority=0.6),	
}


