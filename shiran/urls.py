from django.conf.urls import patterns, include, url
from django.contrib import admin
from shiran import views

from django.conf import settings
from django.conf.urls.static import static

from pages.views import detail as page_detail
from blog.views import display_post, portfolio

from django.contrib.sitemaps.views import sitemap
from .sitemaps import sitemaps as sm


urlpatterns = patterns('',
	url(r'^admin/history/', include('history.urls')),
	url(r'^admin/', include(admin.site.urls)),
	url(r'^$', views.splash),
	url(r'^blog/', include('blog.urls')),
	url(r'^people/', include('people.urls')),
	url(r'^events/', include('events.urls')),
	url(r'^photos/', include('photos.urls')),
	url(r'^videos/', include('videos.urls')),
	url(r'^music/', include('music.urls')),
	url(r'^map/', include('map.urls')),

	url(r'^portfolio/(?P<slug>[A-Za-z0-9_\-\.]+)/$', display_post, name='blog-post-detail'),
	url(r'^portfolio', portfolio, name='portpage'),
	url(r'^events/', include('events.urls')),
    url(r'^s3direct/', include('s3direct.urls')),
  	url(r'^', include('password_reset.urls')),
    url(r'^sitemap\.xml', sitemap, {'sitemaps': sm},name='django.contrib.sitemaps.views.sitemap')


) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


#for dynamic cms pages
urlpatterns += patterns('',
    url(r'^(?P<page_url>[A-Za-z0-9\/_\-\.]+)/$', page_detail)
)



