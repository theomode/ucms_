from django.conf.urls import patterns, url
from views import *


urlpatterns = patterns('',
	url(r'^$', blog, name='blog'),
 	url(r'^search/$', search, name='blog-search'),
	url(r'^tag/(?P<tag_name>[A-Za-z0-9_\-\.]+)/$', display_tag, name='posts-by-tag'),
	url(r'^category/(?P<cat_name>[A-Za-z0-9_\-\.]+)/$', display_cat, name='posts-by-cat'),
	url(r'archive', archive_list),
	url(r'^(?P<slug>[A-Za-z0-9_\-\.]+)/$', display_post, name='blog-post-detail'),

)