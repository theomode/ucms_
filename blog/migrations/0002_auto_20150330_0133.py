# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
        ('people', '0001_initial'),
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='people',
            field=models.ManyToManyField(related_name='people', null=True, to='people.People', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='post',
            name='project',
            field=models.ManyToManyField(related_name='projects', null=True, to='sites.Site', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='post',
            name='tags',
            field=models.ManyToManyField(related_name='tag', null=True, to='blog.Tag', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='feat',
            name='post',
            field=models.ForeignKey(related_name='featured post', to='blog.Post'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='feat',
            name='project',
            field=models.ForeignKey(related_name='featured site', to='sites.Site'),
            preserve_default=True,
        ),
    ]
