# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0001_initial'),
        ('blog', '0003_remove_post_feat'),
    ]

    operations = [
        migrations.AddField(
            model_name='feat',
            name='people',
            field=models.ManyToManyField(related_name='featpeople', null=True, to='people.People', blank=True),
            preserve_default=True,
        ),
    ]
