# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0006_post_snapshot_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='video',
            field=models.TextField(help_text=b'ploe', null=True, blank=True),
            preserve_default=True,
        ),
    ]
