# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import s3direct.fields


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Feat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('main_image', s3direct.fields.S3DirectField(help_text=b'<p>Width:560px Height:300px</p>', blank=True)),
                ('feat_image', s3direct.fields.S3DirectField(help_text=b'<p>Width:1920px Height:800px</p>', blank=True)),
                ('credit', models.CharField(max_length=255, null=True, blank=True)),
                ('title', models.CharField(max_length=255, null=True, blank=True)),
                ('page_url', models.SlugField(null=True, blank=True)),
                ('author', models.CharField(max_length=255, null=True, blank=True)),
                ('author_link', models.URLField(null=True, blank=True)),
                ('date', models.DateField(null=True, blank=True)),
                ('summary', models.TextField(null=True, blank=True)),
                ('body', models.TextField(help_text=b'ckeditor', null=True, blank=True)),
                ('video', models.TextField(null=True, blank=True)),
                ('audio', models.TextField(null=True, blank=True)),
                ('link', models.URLField(null=True, blank=True)),
                ('feat', models.BooleanField(default=False)),
                ('origin', models.ForeignKey(related_name='origin', blank=True, to='sites.Site', null=True)),
            ],
            options={
                'ordering': ['-date'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
