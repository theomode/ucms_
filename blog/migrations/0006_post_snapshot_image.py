# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import s3direct.fields


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0005_auto_20150414_0544'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='snapshot_image',
            field=s3direct.fields.S3DirectField(default='', help_text=b'<p>Width:560px Height:300px</p>', blank=True),
            preserve_default=False,
        ),
    ]
