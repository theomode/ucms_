from django.db import models
from datetime import datetime
from django.db.models.query import QuerySet
from django.template.defaultfilters import slugify
#from autoslug import AutoSlugField
#from settings import MEDIA_URL
from s3direct.fields import S3DirectField

from django.contrib.sites.models import Site
from people.models import People

class Tag(models.Model):
	name 		= models.CharField(max_length=255,blank=True, null=True)

	def __unicode__(self):
		return self.name

	def get_absolute_url(self):
		current_site = Site.objects.get_current().domain
		return  '/tag/' + self.name

class Category(models.Model):
	name 		= models.CharField(max_length=255,blank=True, null=True)

	def __unicode__(self):
		return self.name

	def get_absolute_url(self):
		current_site = Site.objects.get_current().domain
		return  '/category/' + self.name
	


class Post(models.Model):
	
	main_image	= S3DirectField(dest='imgs', blank=True, help_text="<p>Width:560px Height:300px</p>")
	snapshot_image = S3DirectField(dest='imgs', blank=True, help_text="<p>Width:560px Height:300px</p>")
	feat_image	= S3DirectField(dest='imgs', blank=True, help_text="<p>Width:1920px Height:800px</p>")
	credit 		= models.CharField(max_length=255,blank=True, null=True)
	title 		= models.CharField(max_length=255,blank=True, null=True)
	page_url 	= models.SlugField(blank=True, null=True) ##sluggyy
	author 		= models.CharField(max_length=255,blank=True, null=True)
	author_link = models.URLField(blank=True, null=True)
	
	date 		= models.DateField(blank=True, null=True)
		
	summary		= models.TextField(blank=True, null=True)
	body		= models.TextField(blank=True, null=True, help_text="ckeditor") 
	
	video		= models.TextField(blank=True, null=True, help_text="ploe")
	audio		= models.TextField(blank=True, null=True)
	#pics		= models.TextField(blank=True, null=True)
	link 		= models.URLField(blank=True, null=True)
	origin		= models.ForeignKey(Site,blank=True,null=True, related_name="origin")
	project		= models.ManyToManyField(Site,blank=True, null=True,related_name="projects")
	#additional festivals
	#draft = models.BooleanField(default=False, help_text="Draft posts will not be visible in the public blog.")
	category 	= models.ForeignKey(Category, related_name="cat",blank=True, null=True)
	tags 		= models.ManyToManyField(Tag, related_name="tag",blank=True, null=True)
	people		= models.ManyToManyField(People, related_name="people",blank=True, null=True)


	def __unicode__(self):
		return self.title

	def title_url(self):
		return self.title.replace(" ","-")
	
	def get_absolute_url(self):
		current_site = Site.objects.get_current().domain
		return  '/blog/' + self.page_url
	
	# def save(self, *args, **kwargs):
	# 	if not self.summary:
	# 		self.summary = self.body[:300]
	# 	super(Post, self).save(*args, **kwargs)
	
	class Meta:
		ordering = ['-date']

class Feat(models.Model):
	project = models.ForeignKey(Site, related_name="featured site")
	post = models.ForeignKey(Post, related_name="featured post")
	people = models.ManyToManyField(People, related_name="featpeople",blank=True, null=True)

	def __unicode__(self):
		return self.project.name



