from django.contrib import admin, messages
from blog.models import Post, Tag, Feat, Category
from django import forms
from django.db import models

from django.conf.urls import patterns
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect

from django.contrib.sites.models import Site

import reversion

class PostAdmin(reversion.VersionAdmin):
	list_display = ['title', 'date', 'id']
	search_fields = ['title', 'body']
	list_filter = ['date','tags','category', 'project']
	ordering = ('-date',)
	filter_horizontal = ('people', 'tags', 'project')
	change_form_template = 'admin/blog/change_form.html'
	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})},}
	class Media:
		js = ('ckeditor/ckeditor.js','admin/admin.js',)

	
	def get_queryset(self, request):
		qs = Post.objects.all()
		if request.user.is_superuser:
			return qs
		else:
			sites = []
	    	for s in request.user.groups.all():
	    		try:
	    			s = Site.objects.get(name=s)
	    		except Site.DoesNotExist:
	    			s = ''
	    		sites.append(s)
			return qs.filter(project__in=sites).distinct()
  
	def get_urls(self):
		urls = super(PostAdmin, self).get_urls()
		my_urls = patterns('',
			(r'(?P<id>[0-9]+)/preview/$', self.admin_site.admin_view(self.preview)), # blog post preview
		)
		return my_urls + urls


	def preview(self, request, id):
		post = get_object_or_404(Post, pk=id)
		context = {'post': post}
		return render_to_response('blog/blog_post.html', context, context_instance=RequestContext(request))

	# add 'preview' post-save option
	def response_change(self, request, obj):
		res = super(PostAdmin, self).response_change(request, obj)
		if "_preview" in request.POST:
			storage = messages.get_messages(request)
			storage.used = True
			return HttpResponseRedirect('/admin/blog/post/%s/preview' % (obj.pk,))
		else:
			return res


class TagAdmin(reversion.VersionAdmin):
	pass

class CategoryAdmin(reversion.VersionAdmin):
	pass
	

class FeatAdmin(reversion.VersionAdmin):
	# form = PostModelForm
	# change_form_template = 'admin/blog/change_form.html'
	filter_horizontal = ('people',)
	
	def get_queryset(self, request):
		qs = Feat.objects.all()
		if request.user.is_superuser:
			return qs
		else:
			sites = []
	    	for s in request.user.groups.all():
	    		try:
	    			s = Site.objects.get(name=s)
	    		except Site.DoesNotExist:
	    			s = ''
	    		sites.append(s)
			return qs.filter(project__in=sites).distinct()
  
admin.site.register(Post, PostAdmin)
admin.site.register(Feat, FeatAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(Category, CategoryAdmin)
