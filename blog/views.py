#import re
#from collections import OrderedDict
from datetime import datetime
# from datetime import date, datetime

from django.shortcuts import render_to_response, render, get_object_or_404
from django.template import RequestContext
from django.http import Http404

from django.contrib.sites.shortcuts import get_current_site

from models import Post, Tag, Feat, Category

def get_cats(request):
	project = get_current_site(request)
	posts = Post.objects.filter(project=project)
	cat_filter = {}
	
	for p in posts: 
		if p.category:
			if p.category not in cat_filter:
				cat_filter[p.category] = 0
			cat_filter[p.category] += 1

	import operator, collections
	cat_names = ['all']

	for i in range(0,len(cat_filter)):#get top 5
		if i >= 5:
			break
		cat = max(cat_filter.iteritems(), key=operator.itemgetter(1))[0]
		cat_names.append(cat)
		cat_filter[cat] = 0

	return cat_names

def portfolio(request):
	project = get_current_site(request)

	try:		
		portfolio = Category.objects.get(name__iexact="portfolio")
		posts = Post.objects.filter(project=project,category=portfolio)
	except Category.DoesNotExist:
		raise Http404
	
	context = {"posts": posts}

	return render_to_response('pages/portfolio.html', context, context_instance=RequestContext(request))


def blog(request):
	project = get_current_site(request)

	posts = Post.objects.filter(project=project)
	more_posts = Post.objects.filter(project=project)[6:]


	if posts.count() == 0:
		raise Http404
	if posts.count() > 6:
		first_posts = posts[:6]
	else:
		first_posts = posts

	try:
		feat_post = Feat.objects.filter(project=project)[0].post
	except IndexError:
		raise Http404

	
	context = {"more_posts": more_posts, "feat": feat_post, "first_posts": first_posts, "cat_names": get_cats(request), "current_cat": "all"}

	return render_to_response('blog/blog.html', context, context_instance=RequestContext(request))

	# return render(request, 'pages/splash.html', context)

def display_tag(request, tag_name):
	project = get_current_site(request)
	
	try:
		i = Tag.objects.get(name__iexact=tag_name.replace('-',' ')).pk
		posts = Post.objects.filter(project=project).filter(tags=i).distinct().order_by('-date')
	except Tag.DoesNotExist:
		posts = []

	first_posts = posts[:6]
	more_posts = posts[6:]

	try:
		feat_post = Feat.objects.filter(project=project)[0].post
	except IndexError:
		raise Http404

	return render(request, 'blog/blog.html', {"feat": feat_post, "first_posts": first_posts, "more_posts": more_posts, "current":  "Posts with tag <span>" + tag_name + "</span>"})

def display_cat(request, cat_name):
	project = get_current_site(request)
	cat_name = cat_name.replace('-',' ')
	try:
		i = Category.objects.get(name__iexact=cat_name).pk
		posts = Post.objects.filter(project=project).filter(category=i).distinct().order_by('-date')
	except Category.DoesNotExist:
		posts = []

	first_posts = posts[:6]
	more_posts = posts[6:]

	try:
		feat_post = Feat.objects.filter(project=project)[0].post
	except IndexError:
		raise Http404

	return render(request, 'blog/blog.html', {"feat": feat_post, "first_posts": first_posts, "more_posts": more_posts, "current":  "Posts with cateogry <span>" + cat_name + "</span>"})


def search(request, query=None):
	query = query or request.GET.get('query', 'puppies')
	posts = Post.filter(draft=False) #contains title, summmary etc. order by -date
	context = {'search_query': query, 'posts': posts}
	return render(request, 'blog/search-results.html', context)

def display_post(request, slug):
	try:
		post = Post.objects.get(page_url=slug)
	except Post.DoesNotExist:
		raise Http404

	project = get_current_site(request)
	related = Post.objects.filter(project=project).exclude(page_url=post.page_url)[:4]
	pk = post.pk
	try:
		left = Post.objects.get(pk=pk-1)
	except Post.DoesNotExist:
		left = ''
	try:
		right = Post.objects.get(pk=pk+1)
	except Post.DoesNotExist:
		right = ''

	link = "http://" + project.name + "/blog/" + post.page_url

	context = {'post': post, 'related': related, 'left':left, 'right':right, 'link':link}

	return render(request, 'blog/blog_post.html', context)

def archive_list(request):
	month_names = ['jan', 'feb', 'mar', 'april', 'may', 'jun', 'july', 'aug', 'sept', 'oct', 'nov', 'dec']
	dates_posted = Post.objects.date('date', 'month', order='DESC')
	archive = OrderedDict()
	for date_published in dates_posted:
		archive[date_published.year] = archive.get(date_published.year, [])
		archive[date_published.year].append({'name': month_names[date_published.month-1], 'number': date_published.month})

	return render_to_response('blog/archive.html', {'archive': archive}, context_instance=RequestContext(request))
