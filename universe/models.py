from django.db import models
from datetime import date
from django import forms

from s3direct.fields import S3DirectField

from django.contrib.sites.models import Site
from django.contrib.sites.shortcuts import get_current_site


class Location(models.Model):
	city = models.CharField(max_length=255,blank=True, null=True) 
	country = models.CharField(max_length=255,blank=True, null=True)
	longitude = models.FloatField(null=True)
	latitude = models.FloatField(null=True)

	def __unicode__(self):
		return self.city + ", " + self.country

	class Meta:
		ordering = ('country',)
	
class Link(models.Model):
	text		= models.CharField(max_length=255)
	url			= models.CharField(max_length=255, help_text="Include / if this is internal. Include http://www. if external.")
	position	= models.IntegerField(max_length=255,blank=True, null=True, help_text="Used for Submenus")

	class Meta:
		ordering = ('position',)

	def __unicode__(self):
		return self.text + ' '+  self.url


class Logo(models.Model): #rename to logo
	name 		= models.CharField(max_length=255,blank=True, null=True)
	image		= S3DirectField(dest='imgs', blank=True, help_text="<p>Width:560px Height:300px</p>")


	def __unicode__(self):
		return self.name

class Upload(models.Model):
	name = models.CharField(max_length=255,blank=True, null=True) 
	_file = S3DirectField(dest='imgs', verbose_name="file", help_text="<p>Width:560px Height:300px</p>")

	
	def __unicode__(self):
		return self.name

class Theme(models.Model):
	name 		= models.CharField(max_length=255,blank=True, null=True)
	css			= models.TextField(blank=True, null=True)
	project 	= models.ForeignKey(Site)

	def __unicode__(self):
		return self.name
	
	