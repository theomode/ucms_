# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('universe', '0007_remove_link_color'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='logo',
            options={},
        ),
        migrations.RemoveField(
            model_name='logo',
            name='position',
        ),
        migrations.RemoveField(
            model_name='logo',
            name='size',
        ),
    ]
