# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('universe', '0002_auto_20150331_0522'),
    ]

    operations = [
        migrations.AlterField(
            model_name='link',
            name='url',
            field=models.CharField(help_text=b'Include / if this is internal. Include http://www. if external.', max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
    ]
