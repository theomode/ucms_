# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import s3direct.fields


class Migration(migrations.Migration):

    dependencies = [
        ('universe', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='logo',
            name='pic',
        ),
        migrations.RemoveField(
            model_name='logo',
            name='url',
        ),
        migrations.AddField(
            model_name='logo',
            name='image',
            field=s3direct.fields.S3DirectField(default='', help_text=b'<p>Width:560px Height:300px</p>', blank=True),
            preserve_default=False,
        ),
    ]
