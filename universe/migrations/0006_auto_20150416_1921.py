# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('universe', '0005_upload'),
    ]

    operations = [
        migrations.AlterField(
            model_name='link',
            name='color',
            field=models.CharField(help_text=b'Used for Buttons', max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='link',
            name='text',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='link',
            name='url',
            field=models.CharField(default='', help_text=b'Include / if this is internal. Include http://www. if external.', max_length=255),
            preserve_default=False,
        ),
    ]
