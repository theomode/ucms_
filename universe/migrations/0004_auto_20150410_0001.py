# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('universe', '0003_auto_20150409_2107'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='link',
            options={'ordering': ('position',)},
        ),
        migrations.AddField(
            model_name='link',
            name='position',
            field=models.IntegerField(help_text=b'Used for Submenus', max_length=255, null=True, blank=True),
            preserve_default=True,
        ),
    ]
