# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('universe', '0010_auto_20150514_1344'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Theming',
            new_name='Theme',
        ),
    ]
