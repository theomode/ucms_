# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import s3direct.fields


class Migration(migrations.Migration):

    dependencies = [
        ('universe', '0004_auto_20150410_0001'),
    ]

    operations = [
        migrations.CreateModel(
            name='Upload',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, null=True, blank=True)),
                ('_file', s3direct.fields.S3DirectField(help_text=b'<p>Width:560px Height:300px</p>', verbose_name=b'file')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
