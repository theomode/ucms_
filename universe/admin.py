from django.contrib import admin
from universe.models import Link,Logo, Location, Upload, Theme

import reversion 

class LinkAdmin(reversion.VersionAdmin):
	list_display = ['text', 'url', 'id']
	search_fields = ['text']
	

class LogoAdmin(reversion.VersionAdmin):
	search_fields = ['name']

class LocationAdmin(reversion.VersionAdmin):
	list_display = ['city', 'country', 'longitude', 'latitude', 'id']
	search_fields = ['city', 'country']
	list_filter = ['city','country']
	
class UploadAdmin(reversion.VersionAdmin):
	list_display = ['name', '_file']
	search_fields = ['name', '_file']
	

admin.site.register(Link,LinkAdmin)
admin.site.register(Location,LocationAdmin)
admin.site.register(Logo,LogoAdmin)
admin.site.register(Upload,UploadAdmin)
admin.site.register(Theme)
