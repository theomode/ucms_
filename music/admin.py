from django.contrib import admin
from music.models import Wave, Featured
# Register your models here.
from django.db import models
from django import forms
from django.conf.urls import patterns
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect


import reversion



class WaveAdmin(reversion.VersionAdmin):
	list_display = ['title', 'date', 'id']
	search_fields = ['title', 'about', 'project']
	list_filter = ['date', 'project', 'artists']
	ordering = ('-date',)
	filter_horizontal = ('project', 'artists')

	def get_urls(self):
		urls = super(WaveAdmin, self).get_urls()
		my_urls = patterns('',
			(r'(?P<id>[0-9]+)/preview/$', self.admin_site.admin_view(self.preview)), # blog post preview
		)
		return my_urls + urls


	def preview(self, request, id):
		wave = get_object_or_404(Wave, pk=id)
		context = {'wave': wave}
		return render_to_response('music/wave.html', context, context_instance=RequestContext(request))

	# add 'preview' post-save option
	def response_change(self, request, obj):
		res = super(WaveAdmin, self).response_change(request, obj)
		if "_preview" in request.POST:
			storage = messages.get_messages(request)
			storage.used = True
			return HttpResponseRedirect('/admin/music/wave/%s/preview' % (obj.pk,))
		else:
			return res

	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})},}
	class Media:
		js = ('ckeditor/ckeditor.js','admin/admin.js',)


class FeaturedAdmin(reversion.VersionAdmin):
	filter_horizontal = ('people',)

admin.site.register(Wave,WaveAdmin)
admin.site.register(Featured,FeaturedAdmin)
