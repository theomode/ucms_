# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0001_initial'),
        ('music', '0002_auto_20150330_0133'),
    ]

    operations = [
        migrations.AddField(
            model_name='featured',
            name='people',
            field=models.ManyToManyField(related_name='wavefeatpeople', null=True, to='people.People', blank=True),
            preserve_default=True,
        ),
    ]
