# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import s3direct.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Featured',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Wave',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('date', models.DateTimeField()),
                ('about', models.TextField(help_text=b'ckeditor', blank=True)),
                ('page_url', models.SlugField(help_text=b'yo')),
                ('embed', models.TextField()),
                ('feat_image', s3direct.fields.S3DirectField(help_text=b'<p>Width:1920px Height:800px</p>', blank=True)),
                ('square_image', s3direct.fields.S3DirectField(help_text=b'<p>Width:500px Height:500px</p>')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
