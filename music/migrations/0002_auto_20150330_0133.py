# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
        ('people', '0001_initial'),
        ('music', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='wave',
            name='artists',
            field=models.ManyToManyField(related_name='artist', to='people.People'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='wave',
            name='project',
            field=models.ManyToManyField(to='sites.Site'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='featured',
            name='feat',
            field=models.ForeignKey(to='music.Wave'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='featured',
            name='project',
            field=models.ForeignKey(to='sites.Site'),
            preserve_default=True,
        ),
    ]
