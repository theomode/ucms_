from django.db import models
from s3direct.fields import S3DirectField

from people.models import People
from django.contrib.sites.models import Site


# Create your models here.
class Wave(models.Model):
	title = models.CharField(max_length=255)
	artists = models.ManyToManyField(People, related_name="artist")
	date = models.DateTimeField()
	about = models.TextField(help_text='ckeditor',blank=True)
	page_url = models.SlugField(help_text="yo")
	embed = models.TextField()
	feat_image	= S3DirectField(dest='imgs', blank=True, help_text="<p>Width:1920px Height:800px</p>")
	square_image = S3DirectField(dest='imgs', help_text="<p>Width:500px Height:500px</p>")
	project = models.ManyToManyField(Site)
	

	def get_absolute_url(self):
		return  '/music/' + self.page_url

	def __unicode__(self):
		return self.title

class Featured(models.Model):
	feat = models.ForeignKey(Wave)
	project = models.ForeignKey(Site)
	people	= models.ManyToManyField(People, related_name="wavefeatpeople",blank=True, null=True)

	def __unicode__(self):
		return self.feat.title