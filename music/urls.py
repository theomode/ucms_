from django.conf.urls import patterns, url
from music import views

urlpatterns = patterns('',
	url(r'^$', views.music),
	url(r'^(?P<wave>.+)/', views.wave),
)


