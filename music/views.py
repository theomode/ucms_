from django.shortcuts import render
from music.models import Wave, Featured 
from django.http import Http404

from django.contrib.sites.shortcuts import get_current_site

# Create your views here.
def music(request):
	project = get_current_site(request)
	waves = Wave.objects.all()
	try:
		featured = Featured.objects.filter(project=project)[0].feat
	except IndexError:
		featured = ''
	
	context = { "waves": waves, "featured" : featured}
	
	return render(request, 'music/music.html', context)


def wave(request,wave):
	url = wave
	project = get_current_site(request)
	try:
		wave = Wave.objects.get(page_url=url,project=project)
	except Wave.DoesNotExist:
		raise Http404

	try:
		related = Wave.objects.filter(project=project).exclude(page_url=url)
	except Wave.DoesNotExist:
		related = ''

	link = "http://" + project.name + "/music/" + url

	context = { "wave":wave, "related":related, 'link':link }
	
	return render(request, 'music/wave.html', context)

