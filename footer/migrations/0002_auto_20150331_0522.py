# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('footer', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='footer',
            name='sidebar',
            field=models.TextField(help_text=b'ckeditor', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='footer',
            name='blurb',
            field=models.TextField(help_text=b'ckeditor', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='footer',
            name='project',
            field=models.ForeignKey(default='', to='sites.Site'),
            preserve_default=False,
        ),
    ]
