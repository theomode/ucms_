# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('footer', '0003_remove_footer_logos'),
    ]

    operations = [
        migrations.AddField(
            model_name='footer',
            name='title',
            field=models.TextField(default='', help_text=b''),
            preserve_default=False,
        ),
    ]
