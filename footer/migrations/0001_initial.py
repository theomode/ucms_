# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
        ('universe', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Footer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('blurb', models.TextField(null=True, blank=True)),
                ('newsletter', models.TextField(null=True, blank=True)),
                ('logos', models.ManyToManyField(to='universe.Logo', null=True, blank=True)),
                ('project', models.ForeignKey(blank=True, to='sites.Site', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SocialMedia',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.URLField(null=True, blank=True)),
                ('media_type', models.CharField(max_length=255, choices=[(b'instagram', b'instagram'), (b'facebook', b'facebook'), (b'twitter', b'twitter'), (b'soundcloud', b'soundcloud'), (b'vimeo', b'vimeo'), (b'youtube', b'youtube'), (b'internet', b'internet')])),
                ('footer', models.ForeignKey(related_name='footer social', blank=True, to='footer.Footer', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
