# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('footer', '0002_auto_20150331_0522'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='footer',
            name='logos',
        ),
    ]
