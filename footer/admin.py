from django.contrib import admin

from footer.models import Footer,SocialMedia
from django.db import models
from django import forms
import reversion

class SocialMediaInline(admin.TabularInline):
	model = SocialMedia
	extra = 1

class FooterAdmin(reversion.VersionAdmin):

	inlines = [SocialMediaInline]
	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})},}
	
	class Media:
		js = ('ckeditor/ckeditor.js','admin/admin.js',)


	def get_queryset(self, request):
		qs = Footer.objects.all()
		if request.user.is_superuser:
			return qs
		else:
			sites = []
	    	for s in request.user.groups.all():
	    		try:
	    			s = Site.objects.get(name=s)
	    		except Site.DoesNotExist:
	    			s = ''
	    		sites.append(s)
			return qs.filter(project__in=sites).distinct()		


admin.site.register(Footer,FooterAdmin)