from django.db import models

from universe.models import Logo
from django.contrib.sites.models import Site



class Footer(models.Model):
	title 		= models.TextField(help_text='')
	blurb		= models.TextField(blank=True, null=True,help_text='ckeditor')
	newsletter 	= models.TextField(blank=True, null=True)
	sidebar 	= models.TextField(blank=True, null=True,help_text='ckeditor')
	project 	= models.ForeignKey(Site)
	
	def __unicode__(self):
		return self.title


class SocialMedia(models.Model):
	MEDIA = (
	    ('instagram', 'instagram'),
	    ('facebook', 'facebook'),
	    ('twitter', 'twitter'),
	    ('soundcloud', 'soundcloud'),
	    ('vimeo', 'vimeo'),
	    ('youtube', 'youtube'),
	    ('internet', 'internet'),
	)
	footer		= models.ForeignKey(Footer, related_name='footer social',blank=True, null=True)
	url 		= models.URLField(blank=True, null=True)
	media_type 	= models.CharField(max_length=255,choices=MEDIA)


	def __unicode__(self):
		return  self.media_type + ', ' + self.url


