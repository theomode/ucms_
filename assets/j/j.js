var width = $(window).width()
var post = 0
$(window).resize(function() {
	width = $(window).width()
	crc()
	
})
$(document).ready(function() {
    slideshow()
    videoSwap()
    subnav()
    blog()
    menu()
    crc()
})

$(window).load(function () {
	setFooter()
	window_changes()
	$('#white').fadeOut(400)
})

function menu() {
	
}
function subnav()  {
	$('#usub ul.mob .drop').click(function(){
		$(this).children('ul').slideToggle(300)
	})
}


function slideshow() {

	var current = 0
	var slides = $('.sli .s')
	var max = slides.length        
	if(max == 1) {
		single()
	}
	init() //load first and second

	function single() {
		$('.crc').hide()
		$('.arw').hide()
	}
	//remove active class
	function deactivate() {
	  $('.sli .s.a').removeClass('a')
	  $('.crc .c.a').removeClass('a')
	}

	function activate(i) {
	  var slide =  $('.sli .s:eq('+i+')') 
	  slide.addClass('a') //active 
	  var circ =  $('.crc .c:eq('+i+')') 
	  circ.addClass('a') //active 
	}

	//animate first slide and load next
	function init() {
		var wH = $(window).height();
		// $('.sss_f').css('height', 700);
		activate(current)

	}


	$('.crc .c').click(function() {
		current = $(this).index('.crc .c')
		deactivate()
		activate(current)
	})
	$('.arw.r').click(function() {
		current = (current+1) % max
		deactivate() //hide current
	    activate(current) //activate next
	})

	$('.arw.l').click(function() {
		current = (current-1) % max
	    deactivate() //hide current
	    activate(current) //activate next
	})
	
	
}

function crc(){
	var ow = 0
	$('.page-bar .crc .c:last-child').addClass('l')
	$('.page-bar .crc .c').each(function(){
		ow = ow + $(this).outerWidth(true)

		return ow
	})
	console.log(ow)
	$('.page-bar .crc').css('margin-left', -ow/2)

}

function videoSwap() {
	$('.container.videos .videos').on('click', '.wrp', function(){
		$('.container.videos .featured').html($(this).attr('name'))
		setTimeout(function(){
			$("html, body").animate({ scrollTop: "100px" }, 420);
		}, 300)
		
	})
}

function blog(){
	$('#content .blog-post .post .txt p iframe').parent().addClass('break')
}



function more(num) {
	$('.moreitem').hide()
	$('.show-more').click(function() {
		len = $('.moreitem').length
		post = $('.moreitem.on').length
		i = 0
		while(post < len && i<num) {
			$('.moreitem:eq('+post+')').fadeIn(420).addClass('on')
			post++
			i++
		}
		if(post >= len) {
			$('.show-more').hide()
		}
	})
	$('.show-more').click()
}


function photos() {
	$(window).scroll(function() {
	   if($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
	       $('.show-more').click()
	   }
	})
	

}

function window_changes() {
	$(window).resize(function() {
		setFooter()
	})
}

function setFooter() {
	var bodyHeight = $("body").height();
	var vwptHeight = $(window).height();
	$("footer").css("position","absolute").css("bottom",0);	
}