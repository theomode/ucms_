(function() {
    // Load the script
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js';
    script.type = 'text/javascript';
    document.getElementsByTagName("head")[0].appendChild(script);

    // Poll for jQuery to come into existance
    var checkReady = function(callback) {
        if (window.jQuery) {
            callback(jQuery);
        }
        else {
            window.setTimeout(function() { checkReady(callback); }, 20);
        }
    };

    // Start polling...
    checkReady(function($) {
        $(function() {
            $(document).ready(function () {
                $('textarea').each(function() {
                    var text = $(this).siblings('p').html()
                    refresh = true
                    if(text == undefined || text.indexOf('ckeditor') == -1) {
                        refresh = true
                        console.log($(this))
                        $(this).removeClass('ckeditor')
                        $(this).show()
                        $(this).siblings('#cke_id_body').hide()
                    }                
                    if(refresh) {
                         window.onload = function() {
                            if(!window.location.hash) {
                                window.location = window.location + '#loaded';
                                window.location.reload();
                            }
                        }
                    }
                })
            })
        });
    });
})();


  