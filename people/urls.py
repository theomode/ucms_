from django.conf.urls import patterns, url
from views import role, person, year, people, person_any

urlpatterns = patterns('',
	url(r'^$', people, name='people'),
	url(r'^(?P<role>[0-9_\-\.]+)/(?P<first_name>.+)_(?P<last_name>.+)', person),
	url(r'^(?P<role>[0-9_\-\.]+)/(?P<first_name>.+)_', person),
	url(r'^(?P<role>[0-9_\-\.]+)/(?P<first_name>.+)', person),
	url(r'^(?P<first_name>.+)_(?P<last_name>.+)', person_any),
	url(r'^(?P<role>.+)/(?P<year>[0-9_\-\.]+)', year, name='year'),
	url(r'^(?P<role>.+)/', role, name='role'),	
	url(r'^(?P<first_name>.+)_', person_any),
	#url(r'^(?P<first_name>.+)', person_any),
	
)
