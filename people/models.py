from django.db import models
from datetime import date
from django import forms

from s3direct.fields import S3DirectField

from django.contrib.sites.models import Site

from universe.models import Location


class People(models.Model):

	first_name  = models.CharField(max_length=255,blank=True, null=True) 
	last_name	= models.CharField(max_length=255,blank=True, null=True) 
	location	= models.ForeignKey(Location,blank=True, null=True)
	region 		= models.CharField(max_length=255,blank=True, null=True)
	pic			= S3DirectField(dest='imgs', blank=True)
	snapshot_image	= S3DirectField(dest='imgs', blank=True, help_text="<p>Width:560px Height:300px</p>")
	credit 		= models.CharField(max_length=255,blank=True, null=True)
	bio			= models.TextField(blank=True, null=True,help_text="ckeditor")
	
	def __unicode__(self):
		return self.first_name + " " + self.last_name

	def get_absolute_url(self):
		if self.last_name:
			return  '/people/' + self.first_name + "_" + self.last_name
		else:
			return  '/people/' + self.first_name
	


class Mode(models.Model):
	person		= models.ForeignKey(People, related_name='person',blank=True, null=True)
	discipline	= models.CharField(max_length=255,blank=True, null=True)
	instrument	= models.CharField(max_length=255,blank=True, null=True)
	year		= models.CharField(max_length=4,blank=True, null=True)
	role		= models.CharField(max_length=255,blank=True, null=True)
	project		= models.ForeignKey(Site,blank=True, null=True)


	def get_absolute_url(self):
		return  '/people/' + self.role + '/' + self.year
	
	def __unicode__(self):
		return self.person.first_name + " " + self.person.last_name + "," + self.year



class SocialMedia(models.Model):
	MEDIA = (
	    ('instagram', 'instagram'),
	    ('facebook', 'facebook'),
	    ('twitter', 'twitter'),
	    ('soundcloud', 'soundcloud'),
	    ('vimeo', 'vimeo'),
	    ('youtube', 'youtube'),
	    ('internet', 'internet'),
	)
	person		= models.ForeignKey(People, related_name='socialmedia',blank=True, null=True)
	url 		= models.URLField(blank=True, null=True)
	media_type 	= models.CharField(max_length=255,choices=MEDIA)


	def __unicode__(self):
		return self.person.first_name + " " + self.person.last_name + "," + self.media_type


class Audio(models.Model):
	person		= models.ForeignKey(People, related_name='audio',blank=True, null=True)
	text		= models.CharField(max_length=255,blank=True, null=True)
	embed_code	= models.TextField(help_text="width must be 100% embeded code only",blank=True, null=True)

	def __unicode__(self):
		return self.person.first_name + " " + self.person.last_name + "," + self.text


class Video(models.Model):
	person		= models.ForeignKey(People, related_name='video',blank=True, null=True)
	text		= models.CharField(max_length=255,blank=True, null=True)
	embed_code	= models.TextField(help_text="embeded code only",blank=True, null=True)
	

	def __unicode__(self):
		return self.person.first_name + " " + self.person.last_name + "," + self.text
