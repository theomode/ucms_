# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import s3direct.fields


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
        ('universe', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Audio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.CharField(max_length=255, null=True, blank=True)),
                ('embed_code', models.TextField(help_text=b'width must be 100% embeded code only', null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Mode',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('discipline', models.CharField(max_length=255, null=True, blank=True)),
                ('instrument', models.CharField(max_length=255, null=True, blank=True)),
                ('year', models.CharField(max_length=4, null=True, blank=True)),
                ('role', models.CharField(max_length=255, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='People',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=255, null=True, blank=True)),
                ('last_name', models.CharField(max_length=255, null=True, blank=True)),
                ('region', models.CharField(max_length=255, null=True, blank=True)),
                ('pic', s3direct.fields.S3DirectField(blank=True)),
                ('credit', models.CharField(max_length=255, null=True, blank=True)),
                ('bio', models.TextField(help_text=b'ckeditor', null=True, blank=True)),
                ('location', models.ForeignKey(blank=True, to='universe.Location', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SocialMedia',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.URLField(null=True, blank=True)),
                ('media_type', models.CharField(max_length=255, choices=[(b'instagram', b'instagram'), (b'facebook', b'facebook'), (b'twitter', b'twitter'), (b'soundcloud', b'soundcloud'), (b'vimeo', b'vimeo'), (b'youtube', b'youtube'), (b'internet', b'internet')])),
                ('person', models.ForeignKey(related_name='socialmedia', blank=True, to='people.People', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.CharField(max_length=255, null=True, blank=True)),
                ('embed_code', models.TextField(help_text=b'embeded code only', null=True, blank=True)),
                ('person', models.ForeignKey(related_name='video', blank=True, to='people.People', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='mode',
            name='person',
            field=models.ForeignKey(related_name='person', blank=True, to='people.People', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='mode',
            name='project',
            field=models.ForeignKey(blank=True, to='sites.Site', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='audio',
            name='person',
            field=models.ForeignKey(related_name='audio', blank=True, to='people.People', null=True),
            preserve_default=True,
        ),
    ]
