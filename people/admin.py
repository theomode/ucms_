from django.contrib import admin
from django import forms
from django.db import models

from people.models import People, Mode, SocialMedia, Audio, Video

from django.conf.urls import patterns
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect


from django.contrib.sites.models import Site
from django.contrib.sites.shortcuts import get_current_site

import reversion

class ModeInline(admin.TabularInline):
	model = Mode
	extra = 1


class SocialMediaInline(admin.TabularInline):
	model = SocialMedia
	extra = 1

class AudioInline(admin.TabularInline):
	model = Audio
	max_num = 1

class VideoInline(admin.TabularInline):
	model = Video
	max_num = 1


class PeopleAdmin(reversion.VersionAdmin):
	list_display = ['first_name', 'last_name', 'region', 'location']
	search_fields = ['first_name', 'last_name']
	list_filter = ['location']
	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})},}
	fields = (('first_name', 'last_name', 'location', 'region'), ('pic', 'credit'), ('bio'))
	inlines = [ModeInline,SocialMediaInline,AudioInline,VideoInline]

	def get_queryset(self, request):
		qs = People.objects.all()
		if request.user.is_superuser:
			return qs
		else:
			sites = []
	    	for s in request.user.groups.all():
	    		try:
	    			s = Site.objects.get(name=s)
	    		except Site.DoesNotExist:
	    			s = ''
	    		sites.append(s)
	    	qs = Mode.objects.filter(project__in=sites).distinct()
    		people = []
	    	for p in qs:
				people.append(p.person.pk)
		return People.objects.filter(pk__in=people).distinct()
	def get_urls(self):
		urls = super(PeopleAdmin, self).get_urls()
		my_urls = patterns('',
			(r'(?P<id>[0-9]+)/preview/$', self.admin_site.admin_view(self.preview)), # blog post preview
		)
		return my_urls + urls


	def preview(self, request, id):
		project = get_current_site(request)
		person = get_object_or_404(People, pk=id)
		modes = Mode.objects.filter(person=person,role__iexact=role,project=project)[:1]
		social = SocialMedia.objects.filter(person=person)
		related = Post.objects.filter(people=person)[:4]
		context = {'related': related, 'person':person, 'modes':mode, 'social':social}

		return render_to_response('people/person.html', context, context_instance=RequestContext(request))

	# add 'preview' post-save option
	def response_change(self, request, obj):
		res = super(PeopleAdmin, self).response_change(request, obj)
		if "_preview" in request.POST:
			storage = messages.get_messages(request)
			storage.used = True
			return HttpResponseRedirect('/admin/people/people/%s/preview' % (obj.pk,))
		else:
			return res
			
	class Media:
		js = ('admin/admin.js','ckeditor/ckeditor.js',)
    

class ModeAdmin(reversion.VersionAdmin):
	list_display = ['person', 'discipline', 'instrument','role', 'year', 'project']
	search_fields = ['person', 'role', 'year', 'instrument', 'discipline']
	list_filter = ['project', 'role', 'year', 'instrument', 'discipline']

admin.site.register(People,PeopleAdmin)
admin.site.register(Mode,ModeAdmin)