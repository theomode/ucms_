from django.shortcuts import render
from people.models import People,Mode, SocialMedia
from blog.models import Post
from django.http import Http404

from django.contrib.sites.shortcuts import get_current_site


def people(request):
	project = get_current_site(request)
	people = Mode.objects.filter(project=project).order_by('?')
	roles = roles_html(project)
	context = {'people':people, 'role_nav':roles}
	return render(request, 'people/people.html', context)

def role(request,role):
	try:
		current_site = get_current_site(request)
		people = Mode.objects.filter(role__iexact=role).filter(project=current_site)
	except Mode.DoesNotExist:
		raise Http404
	if len(people) < 1:
		raise Http404
	
	role_nav = role_nav_html(role,current_site)

	context = {'people':people, 'role_nav': role_nav, 'role': role}
	return render(request, 'people/people.html', context)

def year(request,role,year):
	from models import Mode
	try:
		current_site = get_current_site(request)
		people = Mode.objects.filter(role__iexact=role).filter(year=year).filter(project=current_site)
	except Mode.DoesNotExist:
		raise Http404
	if len(people) < 1:
		raise Http404
	
	role_nav = role_nav_html(role,current_site,year)

	context = {'people':people, 'role_nav': role_nav, 'role': role, 'year':year}
	return render(request, 'people/people.html', context)

def person(request,role,first_name,last_name=''):
	project = get_current_site(request)
	try: #mst check for two!!!
		person = People.objects.filter(first_name__iexact=first_name,last_name__iexact=last_name)[0]
	except People.DoesNotExist:
		raise Http404
	try:
		mode = Mode.objects.filter(person=person,role__iexact=role,project=project)[:1]
	except Mode.DoesNotExist:
		raise Http404

	try:
		social = SocialMedia.objects.filter(person=person)
	except SocialMedia.DoesNotExist:
		social = []
	
	related = Post.objects.filter(people=person)[:4]

	link = "http://www." + project.name + "/people/" + first_name + '_' + last_name

	context = {'related': related, 'person':person, 'modes':mode, 'social':social, 'link':link}

	return render(request, 'people/person.html', context)



def person_any(request,first_name,last_name=''):
	project = get_current_site(request)
	try: #mst check for two!!!
		person = People.objects.get(first_name__iexact=first_name,last_name__iexact=last_name)
	except People.DoesNotExist:
		raise Http404
	try:
		mode = Mode.objects.filter(person=person,project=project)[:1]
	except Mode.DoesNotExist:
		raise Http404

	try:
		social = SocialMedia.objects.filter(person=person)
	except SocialMedia.DoesNotExist:
		social = []
	
	link = "http://" + project.name + "/people/" + first_name + '_' + last_name

	related = Post.objects.filter(people=person)[:4]
	context = {'related': related, 'person':person, 'modes':mode, 'social':social, 'link':link}

	return render(request, 'people/person.html', context)


def roles_html(project):
	roles = Mode.objects.filter(project=project).values_list('role',flat=True).distinct()
		
	html = ''
	if len(roles) > 0:
		html +='<div class="row subnav"><ul class="nav nav-pills">'
		html += '<li role="presentation" class="active sub-active-all"><a href="/people">all</a></li>'
		for role in roles:
			html += '<li role="presentation"><a href="/people/'+role+'/">'+role+'</a></li>'
		html += '</ul></div>'
	return html


def role_nav_html(role,project,curent_year=None):
	try:
		years = Mode.objects.filter(project=project).filter(role__iexact=role).values_list('year',flat=True).distinct().order_by('-year')
	except Mode.DoesNotExist:
		raise Http404
	
	html = ''
	if len(years) > 0:
		html +='<div class="row subnav"><ul class="nav nav-pills">'
		if curent_year == None:
			html += '<li role="presentation" class="active sub-active-all"><a href="/people/'+role+'/">all</a></li>'
			curent_year = ''
		else:
			html += '<li role="presentation" class="sub-active-all"><a href="/people/'+role+'/">all</a></li>'
		for year in years:
			if str(year) == str(curent_year):
				html += '<li role="presentation" class="active sub-active-'+year+'"><a href="/people/'+role+'/'+year+'">'+year+'</a></li>'
			else:	
				html += '<li role="presentation" class="sub-active-'+year+'"><a href="/people/'+role+'/'+year+'">'+year+'</a></li>'
		html += '</ul></div>'
	return html
