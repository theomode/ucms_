from django.contrib import admin
from django import forms
from django.db import models

from django.conf.urls import patterns
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect


from pages.models import  Page 

import reversion
class PageAdmin(reversion.VersionAdmin):
	list_filter = ['project']
	filter_horizontal = ('people','project')
	
	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})},}
	class Media:
		js = ('ckeditor/ckeditor.js','admin/admin.js',)


	def get_urls(self):
		urls = super(PageAdmin, self).get_urls()
		my_urls = patterns('',
			(r'(?P<id>[0-9]+)/preview/$', self.admin_site.admin_view(self.preview)), # blog post preview
		)
		return my_urls + urls


	def preview(self, request, id):
		page = get_object_or_404(Page, pk=id)
		context = {'page': page}
		return render_to_response('pages/generic.html', context, context_instance=RequestContext(request))

	# add 'preview' post-save option
	def response_change(self, request, obj):
		res = super(PageAdmin, self).response_change(request, obj)
		if "_preview" in request.POST:
			storage = messages.get_messages(request)
			storage.used = True
			return HttpResponseRedirect('/admin/pages/page/%s/preview' % (obj.pk,))
		else:
			return res




admin.site.register(Page, PageAdmin)