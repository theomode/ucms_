# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0007_auto_20150420_2211'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='page',
            name='name_slug',
        ),
    ]
