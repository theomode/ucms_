# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0002_auto_20150331_0430'),
    ]

    operations = [
        migrations.AlterField(
            model_name='page',
            name='template_url',
            field=models.CharField(default=b'pages/generic.html', max_length=64, verbose_name=b'template name', choices=[(b'pages/generic.html', b'Generic'), (b'pages/splash.html', b'Home'), (b'people/people.html', b'People'), (b'blog/blog.html', b'Blog'), (b'videos/videos.html', b'Videos'), (b'music/music.html', b'Music'), (b'photos/photos.html', b'Photo'), (b'pages/portfolio.html', b'Portfolio')]),
            preserve_default=True,
        ),
    ]
