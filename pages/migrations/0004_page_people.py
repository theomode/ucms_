# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0001_initial'),
        ('pages', '0003_auto_20150331_0442'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='people',
            field=models.ManyToManyField(to='people.People', null=True, blank=True),
            preserve_default=True,
        ),
    ]
