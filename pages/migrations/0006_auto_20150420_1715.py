# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0005_auto_20150418_1309'),
    ]

    operations = [
        migrations.AlterField(
            model_name='page',
            name='template_url',
            field=models.CharField(default=b'pages/generic.html', max_length=64, verbose_name=b'template name', choices=[(b'pages/generic.html', b'Generic')]),
            preserve_default=True,
        ),
    ]
