# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.CharField(default=b'', max_length=255, blank=True)),
                ('title', models.CharField(default=b'', max_length=120)),
                ('name_slug', models.CharField(default=b'', max_length=64, blank=True)),
                ('template_url', models.CharField(default=b'pages/generic.html', max_length=64, verbose_name=b'template name', choices=[(b'pages/generic.html', b'Generic'), (b'pages/splash.html', b'Home'), (b'people/people.html', b'People'), (b'blog/blog.html', b'Blog'), (b'videos/videos.html', b'Videos'), (b'music/music.html', b'Music'), (b'photos/photos.html', b'Photo')])),
                ('content', models.TextField(default=b'', blank=True)),
                ('project', models.ManyToManyField(to='sites.Site', null=True, blank=True)),
            ],
            options={
                'ordering': ['title'],
            },
            bases=(models.Model,),
        ),
    ]
