from datetime import datetime

from blog.models import Post, Tag
from people.models import People

from django.contrib.sites.models import Site

from django.http import Http404


class PageContextLoaderBase(object):
	_request = None

	def __init__(self, request):
		self._request = request

	def get_image_set(self, name):
		try:
			return ImageSet.objects.get(name=name)
		except ImageSet.DoesNotExist:
			return ImageSet()

	def get_context(self):
		return {}


class DefaultPageContextLoader(PageContextLoaderBase):
	pass

class HomePageContextLoader(PageContextLoaderBase):
	def get_context(self):
		return {}

class PeoplePageContextLoader(PageContextLoaderBase):
	def get_context(self):
		people = People.objects.all()
		return {'people':people}

class EventsPageContextLoader(PageContextLoaderBase):
	def get_context(self):
		events = Events.objects.all()
		return {'events':events}

class PortfolioPageContextLoader(PageContextLoaderBase):
	def get_context(self):
		project = Site.objects.get_current()
		try: 
			portfolio =	Tag.objects.get(name='portfolio')
		except Tag.DoesNotExist:
			portfolio = 1
		posts = Post.objects.filter(tags=portfolio,project=project)
		return {'posts':posts}
	




class BlogPageContextLoader(PageContextLoaderBase):
	def _year(self):
		return self._request.GET.get('year', None)

	def _month(self):
		return self._request.GET.get('month', None)

	def _get_author_filter(self):
		author_slug = self._request.GET.get('author', None)
		# try:
		# 	return UserProfile.objects.get(name_slug=author_slug)
		# except UserProfile.DoesNotExist:
		return None

	def _get_category_filter(self):
		category_name = self._request.GET.get('category', None)
		# try:
		# 	return Category.objects.get(slug=category_name)
		# except Category.DoesNotExist:
		return None

	def _get_posts(self, year, month, category, author):
		if category:
			return Post.objects.category(category)
		elif year and month:
			return Post.objects.year(year).month(month)
		elif year:
			return Post.objects.year(year)
		elif author:
			return Post.objects.userprofile(author)
		else:
			return Post.objects.all()

	def get_context(self):
		# from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
		# year, month = self._year(), self._month()
		# category = self._get_category_filter()
		# author = self._get_author_filter()

		# posts = self._get_posts(year, month, category, author)
		# # page = self.get_page_number()
		# # posts_per_page = 10
		# # paginator = Paginator(posts, posts_per_page)

		# # try:
		# # 	posts = paginator.page(page)
		# # except PageNotAnInteger:
		# # 	posts = paginator.page(1)
		# # except EmptyPage:
		# # 	posts = paginator.page(paginator.num_pages)

		# post_filters = {'category': category, 'year': year, 'month': month, 'author': author}
		# return { 'posts': posts , 'post_filters': post_filters}
		posts = Post.objects.all()
		return { 'posts': posts }


