from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import Http404


import context_loaders as loaders
from models import Page


def get_template_context(template_url, request):
	template_map = {
		'pages/splash.html': loaders.HomePageContextLoader,
		'pages/generic.html': loaders.DefaultPageContextLoader,
		'blog/blog.html': loaders.BlogPageContextLoader,
		'people/people.html': loaders.PeoplePageContextLoader,
		'events/events.html': loaders.EventsPageContextLoader,
		'pages/portfolio.html': loaders.PortfolioPageContextLoader,
	}
	context_loader = template_map.get(template_url, loaders.DefaultPageContextLoader)
	return context_loader(request).get_context()

def detail(request, page_url=''):
	root = ''
	try:
		page = Page.objects.get(url=page_url)
	except Page.DoesNotExist:
		raise Http404

	base_context = {'page': page }
	
	page_context = get_template_context(page.template_url, request)
	final_context = dict(base_context.items() + page_context.items())

	return render_to_response(page.template_url, final_context, context_instance=RequestContext(request), content_type='html')
	#content type is html mebe no need to set 


# Create your views here.
