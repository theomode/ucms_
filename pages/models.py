from django.db import models

from people.models import People

from django.contrib.sites.models import Site



class Page(models.Model):
	
	TEMPLATE_CHOICES = (
		('pages/generic.html', 'Generic'),
	)

	url = models.CharField(max_length=255,
						   default='',
						   null=False,
						   blank=True)

	title = models.CharField(max_length=120,
							 default='',
							 null=False,
							 blank=False)

	template_url = models.CharField(max_length=64,
									 default='pages/generic.html',
									 verbose_name='template name',
									 choices=TEMPLATE_CHOICES)

	project = models.ManyToManyField(Site,null=True,blank=True)

	people = models.ManyToManyField(People,null=True,blank=True)


	content = models.TextField(default='',blank=True, help_text="ckeditor")



	def __unicode__(self):
		return self.title
	
	def get_absolute_url(self):
		return  '/' + self.url

	class Meta:
		ordering = ['title']




