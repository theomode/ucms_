from django.conf.urls import patterns, url
from views import detail

urlpatterns = patterns('pages.views',
	# url(r'^$', index),
	url(r'^(?P<page_url>.*)$', detail),
)
