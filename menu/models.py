from django.db import models
from datetime import date
from django import forms


from django.contrib.sites.models import Site
from django.contrib.sites.shortcuts import get_current_site

from universe.models import Link

class Submenu(models.Model):
	name 		= models.CharField(max_length=255)
	links 		= models.ManyToManyField(Link)
	project 	= models.ForeignKey(Site,blank=True, null=True)
	
	def __unicode__(self):
		return self.name

class Menu(models.Model):
	logo 		= models.URLField(blank=True, null=True)
	title		= models.CharField(max_length=255,blank=True, null=True)
	now			= models.ForeignKey(Link,  blank=True, null=True)
	project 	= models.ForeignKey(Site,blank=True, null=True)

	def __unicode__(self):
		return self.title

	def get_nav_html(self,project,path):
		html = '<div class="collapse navbar-collapse" id="nav-collapse"><ul class="nav navbar-nav">'
		
		for item in MenuItem.objects.filter(menu=self.pk).order_by('position'):
			try: 
				link = Link.objects.filter(url=path)[0]
				sub = Submenu.objects.filter(project=project,links=link)[0]
			except IndexError:
				sub=''

			if item.link:
				if sub and sub.name == item.name:
					html +=  '<li class="nav-'+item.link.text+' uac_b"><a href="'+item.link.url+'">'+item.name+'</a></li>'
				else:
					html +=  '<li class="nav-'+item.link.text+'"><a href="'+item.link.url+'">'+item.name+'</a></li>'

		if self.now:
			html += '<li id="now" ><a href="'+self.now.url+'" class="btn btn-default">'+self.now.text+'</a></li>'
		html += '</ul></div>'
		return html


class MenuItem(models.Model):
	name 		= models.CharField(max_length=255,blank=True, null=True)
	link		= models.ForeignKey(Link, blank=True, null=True, related_name='link')
	position 	= models.IntegerField(blank=True, null=True)
	menu 		= models.ForeignKey(Menu)

	def __unicode__(self):
		return self.name