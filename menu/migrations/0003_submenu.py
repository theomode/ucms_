# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('universe', '0002_auto_20150331_0522'),
        ('menu', '0002_auto_20150330_2155'),
    ]

    operations = [
        migrations.CreateModel(
            name='Submenu',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, null=True, blank=True)),
                ('links', models.ManyToManyField(to='universe.Link')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
