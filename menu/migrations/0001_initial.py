# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
        ('universe', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Dropdown',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DropdownItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, null=True, blank=True)),
                ('people', models.BooleanField(default=False)),
                ('festival', models.BooleanField(default=False)),
                ('dropdown', models.ForeignKey(blank=True, to='menu.Dropdown', null=True)),
                ('link', models.ForeignKey(blank=True, to='universe.Link', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Menu',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('logo', models.URLField(null=True, blank=True)),
                ('title', models.TextField(null=True, blank=True)),
                ('now', models.ForeignKey(blank=True, to='universe.Link', null=True)),
                ('project', models.ForeignKey(blank=True, to='sites.Site', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MenuItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, null=True, blank=True)),
                ('position', models.IntegerField(null=True, blank=True)),
                ('dropdown', models.ForeignKey(blank=True, to='menu.Dropdown', null=True)),
                ('link', models.ForeignKey(related_name='link', blank=True, to='universe.Link', null=True)),
                ('menu', models.ForeignKey(to='menu.Menu')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
