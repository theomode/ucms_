# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('menu', '0006_merge'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='dropdownitem',
            name='dropdown',
        ),
        migrations.RemoveField(
            model_name='dropdownitem',
            name='link',
        ),
        migrations.DeleteModel(
            name='DropdownItem',
        ),
        migrations.RemoveField(
            model_name='menuitem',
            name='dropdown',
        ),
        migrations.DeleteModel(
            name='Dropdown',
        ),
    ]
