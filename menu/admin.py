from django.contrib import admin
from menu.models import Menu, MenuItem, Submenu
# Register your models here.f
import reversion


class MenuItemInline(admin.TabularInline):
	model = MenuItem
	extra = 1


class SubmenuAdmin(reversion.VersionAdmin):
	filter_horizontal = ('links',)
	

class MenuAdmin(reversion.VersionAdmin):
	search_fields = ['name']
	list_filter = ['project']
	
	inlines = [MenuItemInline]

	def get_queryset(self, request):
		qs = Menu.objects.all()
		if request.user.is_superuser:
			return qs
		else:
			sites = []
	    	for s in request.user.groups.all():
	    		try:
	    			s = Site.objects.get(name=s)
	    		except Site.DoesNotExist:
	    			s = ''
	    		sites.append(s)
			return qs.filter(project__in=sites).distinct()


admin.site.register(Menu,MenuAdmin)
admin.site.register(Submenu,SubmenuAdmin)
