from django.shortcuts import render

from people.models import People
from django.http import Http404

import reversion

from django.db import models


from django.contrib.sites.shortcuts import get_current_site

from django.db.models.loading import get_model

def get_models(request):
	model_list = models.get_models()
	out = []
	for m in model_list:
		parse_name = str(m) 
		cut_model = parse_name.rfind('.')
		model_name = parse_name[cut_model+1:-2]
		
		parse_name = parse_name[0:cut_model]

		cut_app = parse_name.rfind('.')
		parse_name = parse_name[0:cut_app]
		
		start = parse_name.rfind('.')+1
		if start  == 0:
			start = parse_name.rfind('\'')+1
		app_name = parse_name[start:]
		
		model = get_model(app_name,model_name)
		old = reversion.get_deleted(model)
		if old.count() > 0:
			for version in old:
				entry = {'version': version, 'app':app_name}
				out.append(entry)
		
	return render(request, 'history/apps.html', {'data': out})

def revert(request,app,model,id):
	model = get_model(app,model)
	deleted = reversion.get_deleted(model)
	revert = deleted.get(id=id)
	revert.revert()
	return get_models(request)


