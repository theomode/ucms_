from django.conf.urls import patterns, include, url

from django.conf import settings

import views 


urlpatterns = patterns('',
	url(r'^$', views.get_models),
	url(r'^(?P<app>.+)/(?P<model>.+)/(?P<id>.+)', views.revert),
	
	
) 