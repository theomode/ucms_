from django.shortcuts import render
from events.models import Event,Type
from django.http import Http404
from django.contrib.sites.shortcuts import get_current_site
import datetime

# Create your views here.
def events(request):
	project = get_current_site(request)
	events = Event.objects.filter(project=project,date__gte=datetime.date.today())
	context = { "events" : events }
	return render(request, 'events/events.html', context)

def display_event(request, slug):
	project = get_current_site(request)
	try:
		event = Event.objects.get(page_url=slug,project=project)	
	except Event.DoesNotExist:
		raise Http404

	pk = event.pk
	try:
		left = Event.objects.get(pk=pk-1)
	except Event.DoesNotExist:
		left = ''
	try:
		right = Event.objects.get(pk=pk+1)
	except Event.DoesNotExist:
		right = ''

	related = Event.objects.all().exclude(page_url=slug)[:4]
	
	
	link = "http://" + project.name + "/events/" + slug

	context = {'event': event, 'related': related, "link": link, "right":right, "left":left}
	return render(request,'events/events_page.html', context)

def display_type(request, slug):
	project = get_current_site(request)
	try:
		tag = Type.objects.get(type_name=slug).pk
	except Type.DoesNotExist:
		raise Http404
	try:
		events = Event.objects.filter(event_type=tag,project=project)	
	except Event.DoesNotExist:
		raise Http404

	context = {'events': events}
	return render(request,'events/events.html', context)

def archive(request):
	project = get_current_site(request)
	events = Event.objects.filter(project=project,date__lt=datetime.date.today()).order_by('date')
	
	context = {'events': events}
	return render(request,'events/archive.html', context)
