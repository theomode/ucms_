from django.conf.urls import patterns, url
#from views import display_tag, display_slug, archive_list, search
from events import views

urlpatterns = patterns('',
	url(r'^$', views.events),
	url(r'^archive', views.archive),
	url(r'^type/(?P<slug>[A-Za-z0-9_\-\.]+)', views.display_type),
	url(r'^(?P<slug>[A-Za-z0-9_\-\.]+)/', views.display_event, name='event-post-detail'),
)

