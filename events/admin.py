from django.contrib import admin
from events.models import Event, Type
# Register your models here.
from django.db import models
from django import forms
from django.conf.urls import patterns
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect


import reversion 


class EventAdmin(reversion.VersionAdmin):
	list_display = ['title', 'date', 'venue', 'event_type' 'id']
	search_fields = ['title', 'body', 'venue', 'event_type']
	list_filter = ['persons','project','location','date', 'event_type']
	list_display = ['title', 'date', 'summary', 'id']
	filter_horizontal = ('persons', 'project')
	
	def get_urls(self):
		urls = super(EventAdmin, self).get_urls()
		my_urls = patterns('',
			(r'(?P<id>[0-9]+)/preview/$', self.admin_site.admin_view(self.preview)),
		)
		return my_urls + urls

	def preview(self, request, id):
		event = get_object_or_404(Event, pk=id)
		context = {'event': event}
		return render_to_response('events/events_page.html', context, context_instance=RequestContext(request))

	# add 'preview' post-save option
	def response_change(self, request, obj):
		res = super(EventAdmin, self).response_change(request, obj)
		if "_preview" in request.POST:
			return HttpResponseRedirect('/admin/events/event/%s/preview' % (obj.pk,))
		else:
			return res


	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})},}
	class Media:
		js = ('ckeditor/ckeditor.js','admin/admin.js',)

class TypeAdmin(reversion.VersionAdmin):
	pass

admin.site.register(Event,EventAdmin)
admin.site.register(Type,TypeAdmin)