from django.db import models
from s3direct.fields import S3DirectField
from django.contrib.sites.models import Site
from people.models import People
from universe.models import Location

# Create your models here.
class Type(models.Model):
	type_name =  models.CharField(max_length=255)


	def get_absolute_url(self):
		return  '/events/type/' + self.type_name

	def __unicode__(self):
		return self.type_name

class Event(models.Model):
	title = models.CharField(max_length=255)
	image = S3DirectField(dest='imgs', blank=True, help_text="<p>Width:560px Height:300px</p>")
	snapshot_image	= S3DirectField(dest='imgs', blank=True, help_text="<p>Width:560px Height:300px</p>")
	credit = models.CharField(max_length=255,blank=True, null=True)
	date = models.DateTimeField()
	venue = models.CharField(max_length=255)
	venue_link = models.URLField(blank=True, null=True)
	location = models.ForeignKey(Location,null=True)
	summary = models.TextField()
	body = models.TextField()
	page_url = models.SlugField()
	ticket_name = models.CharField(max_length=255,blank=True, null=True)
	ticket_url = models.URLField(blank=True, null=True)
	

	event_type	= models.ForeignKey(Type, related_name="type")
	persons		= models.ManyToManyField(People, related_name="persons",blank=True, null=True)
	project		= models.ManyToManyField(Site)

	def __unicode__(self):
		return self.title

	def get_absolute_url(self):
		return  '/events/' + self.page_url
	

	class Meta:
		ordering = ['-date']

