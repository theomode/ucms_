# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import s3direct.fields


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0002_auto_20150330_0133'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='image',
            field=s3direct.fields.S3DirectField(help_text=b'<p>Width:560px Height:300px</p>', blank=True),
            preserve_default=True,
        ),
    ]
