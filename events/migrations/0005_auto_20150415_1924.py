# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0004_event_snapshot_image'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='types',
        ),
        migrations.AddField(
            model_name='event',
            name='event_type',
            field=models.ForeignKey(related_name='type', default=1, to='events.Type'),
            preserve_default=False,
        ),
    ]
