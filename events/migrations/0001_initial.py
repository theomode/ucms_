# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import s3direct.fields


class Migration(migrations.Migration):

    dependencies = [
        ('universe', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.TextField()),
                ('image', s3direct.fields.S3DirectField()),
                ('credit', models.CharField(max_length=255, null=True, blank=True)),
                ('date', models.DateTimeField(null=True, blank=True)),
                ('venue', models.CharField(max_length=255)),
                ('venue_link', models.URLField(null=True, blank=True)),
                ('summary', models.TextField()),
                ('body', models.TextField()),
                ('page_url', models.SlugField()),
                ('ticket_name', models.CharField(max_length=255, null=True, blank=True)),
                ('ticket_url', models.URLField(null=True, blank=True)),
                ('location', models.ForeignKey(to='universe.Location', null=True)),
            ],
            options={
                'ordering': ['-date'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Type',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type_name', models.CharField(max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
