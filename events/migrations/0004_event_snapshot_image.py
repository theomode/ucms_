# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import s3direct.fields


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0003_auto_20150404_1534'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='snapshot_image',
            field=s3direct.fields.S3DirectField(default='', help_text=b'<p>Width:560px Height:300px</p>', blank=True),
            preserve_default=False,
        ),
    ]
