# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
        ('events', '0001_initial'),
        ('people', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='persons',
            field=models.ManyToManyField(related_name='persons', null=True, to='people.People', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='project',
            field=models.ManyToManyField(to='sites.Site'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='event',
            name='types',
            field=models.ManyToManyField(related_name='type', to='events.Type'),
            preserve_default=True,
        ),
    ]
